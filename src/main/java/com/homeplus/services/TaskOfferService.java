package com.homeplus.services;

import com.homeplus.dtos.task.TaskGetDto;
import com.homeplus.dtos.taskOffer.TaskOfferGetDto;
import com.homeplus.dtos.taskOffer.TaskOfferPostDto;
import com.homeplus.email.EmailSender;
import com.homeplus.models.*;
import com.homeplus.repositories.TaskOfferRepository;
import com.homeplus.utility.mapper.TaskOfferMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TaskOfferService {

    private final TaskOfferRepository taskOfferRepository;
    private final TaskOfferMapper taskOfferMapper;
    private final TaskService taskService;
    private final TaskerService taskerService;
    private final UserService userService;
    private final EmailSender emailSender;

    public List<TaskOfferGetDto> getOffersByTaskId(Long id) {
        TaskEntity task = taskService.getTaskEntityById(id);
        return taskOfferRepository.findOffersByTask(task).stream()
                .map(offer -> taskOfferMapper.fromEntity(offer))
                .collect(Collectors.toList());
    }

    public Boolean checkIsOfferByTaskAndTasker(TaskEntity taskEntity, TaskerEntity taskerEntity) {
        return taskOfferRepository.findOfferByTaskAndTasker(taskEntity, taskerEntity).isPresent();
    }

    @Transactional
    public void createTaskOffer(TaskOfferPostDto taskOfferPostDto) {

        TaskEntity task = taskService.getTaskEntityById(taskOfferPostDto.getTask_id());
        TaskerEntity tasker = taskerService.getTaskerEntityById(taskOfferPostDto.getTasker_id());
        if(task.getUserEntity() == tasker.getUserEntity()) {
            throw new IllegalStateException("You cannot apply for your own task");
        }

        Boolean isOffer = checkIsOfferByTaskAndTasker(task, tasker);
        if(isOffer) {
            throw new IllegalStateException("You already made an offer for this task");
        }
        taskOfferPostDto.setTaskEntity(task);
        taskOfferPostDto.setTaskerEntity(tasker);
        TaskOfferEntity taskOffer = taskOfferMapper.postDtoToEntity(taskOfferPostDto);

        taskOfferRepository.save(taskOffer);

        String task_email = task.getUserEntity().getEmail();
        String task_name = task.getUserEntity().getName();
        String email_msg = taskOfferPostDto.getTaskerEntity().getUserEntity().getName() + " has created an offer about your post, please go to Homeplus to check!";
        emailSender.send(task_email, buildOfferEmail(task_name, email_msg));
    }

    public void updateOfferStatus(Long id, String reply_msg, TaskOfferStatus status) {
        taskOfferRepository.acceptOrRejectOffer(id, reply_msg, status, OffsetDateTime.now());

        if (status == TaskOfferStatus.ACCEPTED) {
            TaskOfferEntity offer = taskOfferRepository.findById(id).get();
            taskService.updateTaskerOfTask(offer, offer.getOffered_price(), TaskStatus.assigned);
        }

        if (status == TaskOfferStatus.CANCELED) {
            TaskOfferEntity offer = taskOfferRepository.findById(id).get();
            taskService.updateTaskerOfTask(offer, null, TaskStatus.open);
        }

        TaskOfferEntity offer = taskOfferRepository.findById(id).get();
        TaskerEntity tasker = offer.getTaskerEntity();
        String tasker_email = tasker.getUserEntity().getEmail();
        String tasker_name = tasker.getUserEntity().getName();
        String email_msg = "The client replied to your offer of [" + offer.getTaskEntity().getTitle().toUpperCase() + "] task, please go to Homeplus to check!";
        emailSender.send(tasker_email, buildOfferEmail(tasker_name, email_msg));

    }

    public List<List<TaskOfferGetDto>> getPendingOffers(Long id) {
        List<TaskGetDto> user_tasks = taskService.getTasksByUidAndKeyword(id, "");

        return user_tasks.stream()
                .map(task -> toGetDto(taskOfferRepository.getPendingOffers(taskService.getTaskEntityById(task.getId()))))
                .collect(Collectors.toList());
    }

    public List<TaskOfferGetDto> getAcceptedOffers(Long id) {
        TaskerEntity taskerEntity = taskerService.getTaskerEntityById(id);
        return toGetDto(taskOfferRepository.getAcceptedOffers(taskerEntity));
    }

    public List<TaskOfferGetDto> toGetDto(Optional<TaskOfferEntity> taskOffers) {
        return taskOffers.stream()
                .map(taskOffer -> taskOfferMapper.fromEntity(taskOffer))
                .collect(Collectors.toList());
    }


    public TaskOfferGetDto getOffersById(Long id) {
        return taskOfferMapper.fromEntity(taskOfferRepository.findById(id).get());
    }

    public String buildOfferEmail(String name, String mail_msg) {
        return "<div style=\"font-family:Helvetica,Arial,sans-serif;font-size:16px;margin:0;color:#0b0c0c\">\n" +
                "\n" +
                "<span style=\"display:none;font-size:1px;color:#fff;max-height:0\"></span>\n" +
                "\n" +
                "  <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;min-width:100%;width:100%!important\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n" +
                "    <tbody><tr>\n" +
                "      <td width=\"100%\" height=\"53\" bgcolor=\"#0b0c0c\">\n" +
                "        \n" +
                "        <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;max-width:580px\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\">\n" +
                "          <tbody><tr>\n" +
                "            <td width=\"70\" bgcolor=\"#0b0c0c\" valign=\"middle\">\n" +
                "                <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">\n" +
                "                  <tbody><tr>\n" +
                "                    <td style=\"padding-left:10px\">\n" +
                "                  \n" +
                "                    </td>\n" +
                "                    <td style=\"font-size:28px;line-height:1.315789474;Margin-top:4px;padding-left:10px\">\n" +
                "                      <span style=\"font-family:Helvetica,Arial,sans-serif;font-weight:700;color:#ffffff;text-decoration:none;vertical-align:top;display:inline-block\">Task Notification</span>\n" +
                "                    </td>\n" +
                "                  </tr>\n" +
                "                </tbody></table>\n" +
                "              </a>\n" +
                "            </td>\n" +
                "          </tr>\n" +
                "        </tbody></table>\n" +
                "        \n" +
                "      </td>\n" +
                "    </tr>\n" +
                "  </tbody></table>\n" +
                "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">\n" +
                "    <tbody><tr>\n" +
                "      <td width=\"10\" height=\"10\" valign=\"middle\"></td>\n" +
                "      <td>\n" +
                "        \n" +
                "                <table role=\"presentation\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">\n" +
                "                  <tbody><tr>\n" +
                "                    <td bgcolor=\"#1D70B8\" width=\"100%\" height=\"10\"></td>\n" +
                "                  </tr>\n" +
                "                </tbody></table>\n" +
                "        \n" +
                "      </td>\n" +
                "      <td width=\"10\" valign=\"middle\" height=\"10\"></td>\n" +
                "    </tr>\n" +
                "  </tbody></table>\n" +
                "\n" +
                "\n" +
                "\n" +
                "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">\n" +
                "    <tbody><tr>\n" +
                "      <td height=\"30\"><br></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                "      <td style=\"font-family:Helvetica,Arial,sans-serif;font-size:19px;line-height:1.315789474;max-width:560px\">\n" +
                "        \n" +
                "            <p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\">Hi " + name + ",</p><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\"> " + mail_msg + " </p>" +
                "        \n" +
                "      </td>\n" +
                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                "       <td style=\"font-family:Helvetica,Arial,sans-serif;font-size:19px;line-height:1.315789474;max-width:560px\">\n" +
                "        \n" +
                "           <p style=\"Margin:0 0 5px 0;font-size:16px;line-height:25px;color:#0b0c0c\">Thank you for choosing HomePlus.</p><p style=\"Margin:0 0 20px 0;font-size:16px;line-height:25px;color:#0b0c0c\"> -The HomePlus Team </p>" +
                "        \n" +
                "      </td>\n" +
                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td height=\"30\"><br></td>\n" +
                "    </tr>\n" +
                "  </tbody></table><div class=\"yj6qo\"></div><div class=\"adL\">\n" +
                "\n" +
                "</div></div>";
    }



}


package com.homeplus.dtos.taskOffer;

import com.homeplus.models.TaskEntity;
import com.homeplus.models.TaskOfferStatus;
import com.homeplus.models.TaskerEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TaskOfferPutDto {
    private Long id;

    private TaskerEntity taskerEntity;

    private TaskEntity taskEntity;

    private String changed_to_status;

    private TaskOfferStatus offer_status;

    private String offered_price;

    private String description;

    private String reply_message;

    private OffsetDateTime created_time;

    private final OffsetDateTime updated_time = OffsetDateTime.now();
}

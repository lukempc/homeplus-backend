package com.homeplus.dtos.comment;

import com.homeplus.models.TaskEntity;
import com.homeplus.models.UserEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CommentPostDto {
    private Long user_id;

    private Long task_id;

    private UserEntity userEntity;

    private TaskEntity taskEntity;

    private String message;

    private final OffsetDateTime created_time = OffsetDateTime.now();

    private final OffsetDateTime updated_time = OffsetDateTime.now();
}

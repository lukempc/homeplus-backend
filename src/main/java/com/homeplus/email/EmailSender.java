package com.homeplus.email;

public interface EmailSender {
    void send(String to, String email);
}

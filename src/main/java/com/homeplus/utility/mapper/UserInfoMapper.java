package com.homeplus.utility.mapper;

import com.homeplus.dtos.user.UserGetDto;
import com.homeplus.dtos.user.UserPutDto;
import com.homeplus.models.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserInfoMapper {

    UserGetDto fromEntity(UserEntity userEntity);

    UserEntity putDtoToEntity(UserPutDto userPutDto);
}

package com.homeplus.utility.mapper;

import com.homeplus.dtos.AccountDto;
import com.homeplus.models.ConfirmationTokenEntity;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;

import static java.time.ZoneOffset.UTC;

@Slf4j
@RequiredArgsConstructor
@Component
public class ConfirmationTokenMapper {

    public ConfirmationTokenEntity mapTokenDtoToEntity(AccountDto accountDto) {
        return ConfirmationTokenEntity.builder()
                .email(accountDto.getEmail().toLowerCase())
                .createTime(OffsetDateTime.now(UTC))
                .expireTime(OffsetDateTime.now(UTC).plusHours(1))
                .build();
    }

}

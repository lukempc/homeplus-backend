package com.homeplus.utility.mapper;

import com.homeplus.dtos.follow.FollowGetDto;
import com.homeplus.dtos.follow.FollowPostDto;
import com.homeplus.dtos.follow.FollowPutDto;
import com.homeplus.models.TaskFollowEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface TaskFollowMapper {

    TaskFollowEntity postDtoToEntity(FollowPostDto followPostDto);

    TaskFollowEntity putDtoToEntity(FollowPutDto followPutDto);

    FollowGetDto fromEntity(TaskFollowEntity taskFollowEntity);
}

package com.homeplus.models;

public enum TaskOfferStatus {
    PENDING,
    ACCEPTED,
    REJECTED,
    CANCELED
}
